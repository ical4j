/**
 * Copyright (c) 2009, Ben Fortuna
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  o Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 *  o Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 *  o Neither the name of Ben Fortuna nor the names of any other contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.fortuna.ical4j.validation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.util.CompatibilityHints;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Checks whether the ical4j validation correctly catches the problems and
 * passes the valid calendars from the icalvalidSchema collection from
 * http://icalvalid.wikidot.com/
 * 
 * @author arnouten
 */
public class ValidationBySchemaTest extends TestCase {
	private static final Log LOG = LogFactory
			.getLog(ValidationBySchemaTest.class);
	private static final int BUFFER = 2048;

	
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		CompatibilityHints.setHintEnabled(
				CompatibilityHints.KEY_RELAXED_UNFOLDING, true);
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		CompatibilityHints.clearHintEnabled(
				CompatibilityHints.KEY_RELAXED_UNFOLDING);
		super.tearDown();
	}

	private String name;
	private String textContent;
	private boolean shouldPass;

	public ValidationBySchemaTest(String testMethod, String name, boolean shouldPass,
			String textContent) {
		super(testMethod);
		this.name = name;
		this.shouldPass = shouldPass;
		this.textContent = textContent;
	}

	private static void addToSuite(TestSuite suite, ZipEntry entry, ZipInputStream zip)
			throws IOException, SAXException, ParserConfigurationException
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = dbf.newDocumentBuilder();

		ByteArrayOutputStream dest = new ByteArrayOutputStream();
		int count;
		byte data[] = new byte[BUFFER];

		while ((count = zip.read(data, 0, BUFFER)) != -1) {
			dest.write(data, 0, count);
		}
		dest.flush();
		dest.close();

		Document dom = db.parse(new ByteArrayInputStream(dest.toByteArray()));
		
		addToSuite(suite, entry.getName(), dom);
	}

	private static void addToSuite(TestSuite suite, String name, Document dom) throws IOException {
		LOG.info("Looking at " + name);
		
		Node rule = dom.getFirstChild();
		NodeList nodes = rule.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			String textContent = StringUtils.trim(node.getTextContent());
			if ("pass".equals(node.getNodeName()) || "fail".equals(node.getNodeName()))
			{
				suite.addTest(new ValidationBySchemaTest("testValidateBySchema", name, "pass".equals(node.getNodeName()), textContent));
			}
		}
	}
	
	public void testValidateBySchema() throws IOException
	{
		boolean passed;
		try
		{
			new CalendarBuilder().build(new ByteArrayInputStream(textContent.getBytes())).validate(true);
			passed = true;
		}
		catch (ParserException e)
		{
			passed = false;
		} catch (ValidationException e) {
			passed = false;
		}
		assertEquals("Test result inconsistent for rule " + name, shouldPass, !passed);
	}
	
	public static TestSuite suite() throws IOException, SAXException, ParserConfigurationException {
		TestSuite suite = new TestSuite();
        
		ZipInputStream zip = new ZipInputStream(ValidationBySchemaTest.class
				.getResourceAsStream("/icalvalidSchema_0_4.zip"));

		ZipEntry entry = zip.getNextEntry();

		while (entry != null) {
			if (!entry.isDirectory() && entry.getName().startsWith("icalvalidSchema/rules/"))
			{
				addToSuite(suite, entry, zip);
			}
			entry = zip.getNextEntry();
		}
		
		return suite;
	}
}
